# README #

### What is this repository for? ###

* This is a Quantum GIS project for the purposes of creating system wide and individual bus maps for Birmingham Jefferson County Transit Authority (BJCTA).
* Version 1


### How do I get set up? ###

* [QGIS](http://www.qgis.org/en/site/) is the program used to make and edit the .qgs file. Install QGIS and open the .qgs file with it.

* You might have to install a couple of Python libraries to get it working. The QGIS website provides the necessary info.

### Who do I talk to? ###

* Contact scott.wehby@gmail.com for questions about this QGIS project.